import { useState, useEffect } from "react";

import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "../redux/store";
import { addTodo, toggleTodo } from "../redux/todoSlice";
//Todo app components
import TodoList from "./TodoList";
import Header from "./Header";
//Style
import { Button, Box, Input } from "@mui/material";

export default function TodoApp() {
  //React Hooks
  const [todoDescription, setTodoDescription] = useState("");

  //React Redux Hooks
  const dispatch = useDispatch<AppDispatch>();

  //fecth 1 task for example purpose jusst 1 time
  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/todos/1")
      .then((response) => response.json())
      .then((json) => {
        dispatch(addTodo(json.title));
      });
  }, []);

  return (
    <div className="TodoApp">
      <>
        <Header />

        <Box className="createTodo" sx={{ p: 2 }}>
          <Input
            placeholder="Add a new todo"
            onChange={(e) => setTodoDescription(e.target.value)}
            value={todoDescription}
          ></Input>

          <Button
            variant="contained"
            color="secondary"
            onClick={() => {
              if (todoDescription.length != 0) {
                dispatch(addTodo(todoDescription));
                setTodoDescription("");
              }
            }}
            sx={{ m: 2 }}
          >
            Add TO-DO
          </Button>
        </Box>

        <TodoList />
      </>
    </div>
  );
}
