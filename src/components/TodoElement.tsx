//Redux
import { useDispatch } from "react-redux";
import { AppDispatch } from "../redux/store";
import { toggleTodo, removeTodo } from "../redux/todoSlice";
//Todo types
import { Todo } from "./TodoTypes";
//Style
import {
  Checkbox,
  ListItemButton,
  ListItemText,
  IconButton,
  ListItem,
  ListItemIcon,
  ListItemSecondaryAction,
} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";

const TodoElement = ({ text, completed, id }: Todo) => {
  const dispatch = useDispatch<AppDispatch>();
  return (
    <ListItem key={id}>
      <ListItemIcon>
        <Checkbox
          edge="start"
          value={completed}
          onChange={() => {
            dispatch(toggleTodo(id));
          }}
        />
      </ListItemIcon>
      <ListItemText
        id={id.toString()}
        style={{
          textDecoration: completed ? "line-through" : "none",
        }}
      >
        {text}
      </ListItemText>
      <ListItemSecondaryAction>
        <IconButton
          onClick={() => {
            dispatch(removeTodo(id));
          }}
        >
          <DeleteIcon />
        </IconButton>
      </ListItemSecondaryAction>
    </ListItem>
  );
};
export default TodoElement;
