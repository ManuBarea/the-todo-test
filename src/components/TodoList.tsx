import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "../redux/store";
import { addTodo, toggleTodo } from "../redux/todoSlice";
//Todo app components
import { Todo } from "./TodoTypes";
import TodoElement from "./TodoElement";
//Style
import { Grid, Box, List } from "@mui/material";

export default function TodoList() {
  const todos = useSelector((state: RootState) => state.todos);

  return (
    <Box display="flex" justifyContent="center">
      <List
        className="todoList"
        sx={{ width: "100%", maxWidth: 500, position: "center" }}
      >
        {todos.map((todo) => (
          <TodoElement {...todo} />
        ))}
      </List>
    </Box>
  );
}
