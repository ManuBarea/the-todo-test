import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface Todo {
  id: number;
  text: string;
  completed: boolean;
}
interface IinitialState {
  id: number;
  todos: Todo[];
}

const initialState = {
  id: 0,
  todos: [],
} as IinitialState;

const todoSlice = createSlice({
  name: "todos",
  initialState,
  reducers: {
    addTodo: {
      reducer: (state, action: PayloadAction<Todo>) => {
        state.todos.push({ ...action.payload, id: state.id });
        state.id++;
      },
      prepare: (text: string) => ({
        payload: {
          text,
          completed: false,
        } as Todo,
      }),
    },
    toggleTodo: (state, action: PayloadAction<number>) => {
      const todo = state.todos.find((t) => t.id === action.payload);
      if (todo) {
        todo.completed = !todo.completed;
      }
    },
    removeTodo: (state, action: PayloadAction<number>) => {
      const indexToRemove = state.todos.findIndex(
        (t) => t.id === action.payload
      );
      state.todos.splice(indexToRemove, 1);
    },
  },
});

export const { addTodo, toggleTodo, removeTodo } = todoSlice.actions;
export default todoSlice.reducer;
