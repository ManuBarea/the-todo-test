import React from "react";
import "./App.css";
import TodoApp from "./components/TodoApp";
import { Provider } from "react-redux";
import { store } from "./redux/store";

function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <TodoApp />
      </Provider>
    </div>
  );
}

export default App;
